﻿using System;
using Twilio;

using Twilio.Rest.Api.V2010.Account;
using Microsoft.Extensions.Options;

namespace OneRide.Services.Sms.TwilioSms
{
    public class TwilioSms : ISmsSender
    {
        public TwilioSms(IOptions<TwilioAccountDetails> twilioAccountDetails)
        {
            _twilioAccountDetails = twilioAccountDetails.Value ?? throw new ArgumentException(nameof(twilioAccountDetails));
        }

        private readonly TwilioAccountDetails _twilioAccountDetails;

        public void SendMessage(string body, string toNumber)
        {
            //  add test code here which is listed below
            TwilioClient.Init(_twilioAccountDetails.AccountSid, _twilioAccountDetails.AuthToken);

            var message = MessageResource.Create(
                body: body,
                from: new Twilio.Types.PhoneNumber(_twilioAccountDetails.FromNumber),
                to: new Twilio.Types.PhoneNumber(toNumber)
            );
        }
    }
}