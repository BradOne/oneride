namespace OneRide.Services.Sms.TwilioSms
{
    public class TwilioAccountDetails
    {
        public string AccountSid { get; set; }
        
        public string AuthToken { get; set; }
        
        public string FromNumber { get; set; }
    
    }
}