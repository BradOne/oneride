using System;

namespace OneRide.Services.Sms{
    public interface ISmsSender{
        void SendMessage(string body, string phoneNumber);
    }
}