using System;

namespace RegistrationUserAndLogin._2.Models
{
    public class CardModel
    {
        public string CardOwnerFirstName { get; set; }
        public string CardOwnerLastName { get; set; }
        public string CardNumber { get; set; }
        public long ExpirationYear { get; set; }
        public long ExpirationMonth { get; set; }
        public string CVV2 { get; set; }
    }
}