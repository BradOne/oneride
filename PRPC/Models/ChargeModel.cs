using System;

namespace RegistrationUserAndLogin._2.Models
{
    public class ChargeModel
    {
        public string Amount { get; set; }
        public int CurrencyId { get; set; }
        public string TransactionId { get; set; }
    }
}