using System;

namespace RegistrationUserAndLogin._2.Models
{
    public class CustomerModel
    {
        public string Buyer_Email { get; set; }
        public string CustomerId { get; set; }
    }
}
