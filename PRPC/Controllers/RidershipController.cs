using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RegistrationUserAndLogin._2.Models;
using OneRideLib.Data;
using RegistrationUserAndLogin._2.Data;
using System.ComponentModel.DataAnnotations;

namespace RegistrationUserAndLogin._2.Controllers
{
    public class RidershipController : Controller
    {
        
        Random rnd = new Random();

        private readonly ApplicationDbContext _context;
        public IActionResult Index()
        {
            return View();
        }
        public RidershipController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult RidershipLoginButton(){
            return View();
        }
        [HttpPost]
        public IActionResult CreateBusRide(int BusID,int RiderID)
        {
            var busRideEvent = new RideEvent
            {
                //RideEventID = rideEventID,
                RiderID = RiderID,
                //BusID = BusID, for when BusID's exist
                BusID = rnd.Next(1,10),
                RideTimestamp = DateTime.Now
            };
            // await someMethodCall();
            //db.add(busRideEvent)
            return View(busRideEvent);
        }
    }
}
