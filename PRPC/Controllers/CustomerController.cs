using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RegistrationUserAndLogin._2.Models;
using Stripe.Infrastructure;


namespace RegistrationUserAndLogin._2.Controllers
{
    public class CustomerController : Controller
    {
        private void createCard(CardModel cm)
        {
            Stripe.CreditCardOptions card = new Stripe.CreditCardOptions();
            card.Name = cm.CardOwnerFirstName + " " + cm.CardOwnerLastName;
            card.Number = cm.CardNumber;
            card.ExpYear = cm.ExpirationYear;
            card.ExpMonth = cm.ExpirationMonth;
            card.Cvc = cm.CVV2;
            //Assign Card to Token Object and create Token  
            Stripe.TokenCreateOptions token = new Stripe.TokenCreateOptions();
            token.Card = card;
            Stripe.TokenService serviceToken = new Stripe.TokenService();
            Stripe.Token newToken = serviceToken.Create(token);
        }

        private void createCustomer(CustomerModel cus)
        {
            //Create Customer Object and Register it on Stripe  
            Stripe.CustomerCreateOptions myCustomer = new Stripe.CustomerCreateOptions();
            myCustomer.Email = cus.Buyer_Email;
            myCustomer.SourceToken = cus.CustomerId;
            var customerService = new Stripe.CustomerService();
            Stripe.Customer stripeCustomer = customerService.Create(myCustomer);
        }

        private void createCharge(ChargeModel cha, CustomerModel cus)
        {
            //Create Charge Object with details of Charge  
            var options = new Stripe.ChargeCreateOptions
            {
                Amount = Convert.ToInt32(cha.Amount),
                Currency = cha.CurrencyId == 1 ? "ILS" : "USD",
                ReceiptEmail = cus.Buyer_Email,
                CustomerId = cus.CustomerId,
                Description = Convert.ToString(cha.TransactionId), //Optional  
            };
            //and Create Method of this object is doing the payment execution.  
            var service = new Stripe.ChargeService();
            Stripe.Charge charge = service.Create(options); // This will do the Payment  .
        }


    }
}
