using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RegistrationUserAndLogin.Models;
//using RidershipModel.Models;
using RegistrationUserAndLogin._2.Data;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using OneRideLib.Data;

namespace RegistrationUserAndLogin._2.Controllers
{
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UserController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: User
        public async Task<IActionResult> Index()
        {
            return View(await _context.User.ToListAsync());
        }

        // GET: User/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: User/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,UserName,Password,FirstName,LastName,Email,Mobile")] User user)
        {
            if (ModelState.IsValid)
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: User/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserId,UserName,Password,FirstName,LastName,Email,Mobile")] User user)
        {
            if (id != user.UserId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.UserId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }
/*
        // GET: User/Edit/5
        public async Task<IActionResult> RidershipLoginButton([Bind("RiderID,BusID,RouteID,RideBegin,RideEnd")] TripData td)
        {
            if (ModelState.IsValid)
            {
                _context.Add(td);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(td);
        }
 */
        // GET: User/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _context.User.FindAsync(id);
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.UserId == id);
        }

        // public MessageController(IOptions<TwilioAccountDetails> twilioAccountDetails)
        // {
        //     _twilioAccountDetails = twilioAccountDetails.Value ?? throw new ArgumentException(nameof(twilioAccountDetails));
        // }

        //private readonly TwilioAccountDetails _twilioAccountDetails;

        // public IActionResult SendMessage(string numberReplacer)
        // {
        //     //  add test code here which is listed below
        //     TwilioClient.Init("ACe1158b8a899b26ee03f9be82b6004dde", "e1cdf806506c24970f4910f61ff01ecb");

        //     var message = MessageResource.Create(
        //         body: "Never tell me the odds",
        //         from: new Twilio.Types.PhoneNumber("+18063105247"),
        //         to: new Twilio.Types.PhoneNumber(numberReplacer));

        //     return View();
        // }



        public async Task<IActionResult> SendMessage(int id)
        {
            var user = await _context.User.FirstOrDefaultAsync(m => m.UserId == id);
            return View(user);
        }

        [HttpPost, ActionName("SendMessage")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendMessageConfirmed(int id)
        {
            var user = await _context.User.FindAsync(id);
            string numberReplacer = user.Mobile;
            //  add test code here which is listed below
            TwilioClient.Init("ACe1158b8a899b26ee03f9be82b6004dde", "e1cdf806506c24970f4910f61ff01ecb");

            var message = MessageResource.Create(
                body: "Never tell me the odds",
                from: new Twilio.Types.PhoneNumber("+18063105247"),
                to: new Twilio.Types.PhoneNumber(numberReplacer));
            return RedirectToAction(nameof(Index));
        }
    }
}
