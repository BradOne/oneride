using System;
using OneRideLib;
using OneRideLib.Data;

namespace Tests{
    class LoggerMock : IOneRideLogger{
        public Guid LogRide(int rider, int bus, DateTime ridetimestamp){
            return Guid.NewGuid();
        }

        public Guid LogRide(RideEvent ride){
            return Guid.NewGuid();
        }
        
        public RideEvent GetRideEventById(Guid id){
            return new RideEvent();
        }
        public RideEvent RemoveRideEventById(Guid id){
            return new RideEvent();
        }
    }
}