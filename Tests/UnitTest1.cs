using System;
using Xunit;
using OneRideLib;

namespace Tests
{
    public class UnitTest1
    {
        OneRideLib.OneRideLogger logger;

        public UnitTest1(){
            LoggerMock mock = new LoggerMock();
            logger = new OneRideLib.OneRideLogger(mock);
        }
        
        [Theory,
        InlineData(1,1,"11/1/2011")]
        public void ReturnsValidGUID(int rider, int bus, string ridetimestamp)
        {
            Guid output = logger.LogRide(rider, bus, DateTime.Parse(ridetimestamp));
            Console.WriteLine(output.ToString());
            Assert.False(output == null || output == Guid.Empty);
        }
    }
}
