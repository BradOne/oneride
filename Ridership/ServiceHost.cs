using Microsoft.Extensions.DependencyInjection;

namespace OneRideLib
{
    public class ServiceHost
    {

        public static ServiceProvider OneRideServiceContainer {get; set; }

        static ServiceHost(){
            OneRideServiceContainer = new ServiceCollection()
                .AddScoped<IOneRideLogger, OneRideMySQLLogger>()
                .BuildServiceProvider();
        }
    }
}