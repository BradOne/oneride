using System;
using OneRideLib.Data;

namespace OneRideLib
{
    public interface IOneRideLogger
    {
        Guid LogRide(int rider, int bus, DateTime ridetimestamp);

        Guid LogRide(RideEvent ride);
        
        RideEvent GetRideEventById(Guid id);
        RideEvent RemoveRideEventById(Guid id); 
    }
}