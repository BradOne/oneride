using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using OneRideLib.Data;

namespace OneRideLib
{
    public class OneRideMySQLLogger : IOneRideLogger
    {

        public OneRideMySQLLogger(){

        }

        public Guid LogRide(int rider, int bus, DateTime ridetimestamp)
        {

            Guid rideEventId = Guid.NewGuid();

            using(OneRideContext db = new OneRideContext()){

                //create new RideEvent
                var ride = new RideEvent {
                    RideEventID     = rideEventId,
                    RiderID         = new Random().Next(1, 100),
                    RideTimestamp   = DateTime.Now
                };

                //write
                db.RideEvents.Add(ride);

                //save
                db.SaveChanges();
            }

            return rideEventId;
        }

        public Guid LogRide(RideEvent ride)
        {

            using(OneRideContext db = new OneRideContext()){

                //write
                db.RideEvents.Add(ride);

                //save
                db.SaveChanges();                
            }

            return ride.RideEventID;
        }

        public RideEvent GetRideEventById(Guid id)
        {

            RideEvent ride = null;

            using(OneRideContext db = new OneRideContext()){

                //find
                ride = db.RideEvents.Find(id);
            }

            return ride;
        }

        public RideEvent RemoveRideEventById(Guid id)
        {

            EntityEntry<RideEvent> ride = null;

            using(OneRideContext db = new OneRideContext()){

                //find
                //ride = db.RideEvents.Find(id);

                //clean up
                ride = db.RideEvents.Remove(db.RideEvents.Find(id));
            }

            return ride.Entity;
        }
    }
}