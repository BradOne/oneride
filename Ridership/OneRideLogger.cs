using System;
using OneRideLib.Data;

namespace OneRideLib
{
    public class OneRideLogger
    {
        private readonly IOneRideLogger _oneRideLogger;
        public OneRideLogger(IOneRideLogger oneRideLogger){
            _oneRideLogger = oneRideLogger;
        }

        public Guid LogRide(int rider, int bus, DateTime ridetimestamp){
            return _oneRideLogger.LogRide(rider, bus, ridetimestamp);
        }

        public Guid LogRide(RideEvent ride){
            return _oneRideLogger.LogRide(ride);
        }
        public RideEvent GetRideEventById(Guid id){
            return _oneRideLogger.GetRideEventById(id);
        }

        public RideEvent RemoveRideEventById(Guid id){
            return _oneRideLogger.RemoveRideEventById(id);
        }
    }
}