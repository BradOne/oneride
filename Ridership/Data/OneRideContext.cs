using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
//using dotenv.net;

using OneRideLib;
using OneRideLib.Data;

public class OneRideContext : DbContext
{
    public DbSet<RideEvent> RideEvents { get; set; }
/* 
    public OneRideContext()
    {
        DotEnv.Config(true, @"D:\jeff\Dropbox\CIS\CIDM\CIDM4390\2019-Spring\Code\TestingDemo\.env");
    }
*/
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseMySql(Environment.GetEnvironmentVariable("ONERIDE_MYSQL_CONNECTION_STRING"), // replace with your Connection String
                        mySqlOptions =>
                        {
                            mySqlOptions.ServerVersion(new Version(5, 7, 25), ServerType.MySql) // replace with your Server Version and Type
                                        .DisableBackslashEscaping();
                        });
    }
}