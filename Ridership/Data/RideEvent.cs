﻿using System;

namespace OneRideLib.Data
{
    public class RideEvent
    {
        public Guid RideEventID { get; set; }
        public int RiderID{ get; set; }

        public int BusID { get; set; }
        
        public DateTime RideTimestamp { get; set; }
    }
}
